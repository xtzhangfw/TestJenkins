package main

import (
	"testing"
)

func TestMyFunc(t *testing.T) {
	result := MyFunc()
	if result == 0 {
		return
	}
	t.Errorf("MyFunc expect 0, actual %d", result)
}
